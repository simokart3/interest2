package models

type Stocks struct {
	ProductId uint    `json:"productid"`
	StockId   uint    `json:"stockid"`
	Qty       float32 `gorm:"type:decimal(12,4);" json:"qty" `
	IsInStock uint    `json:"isinstock" `
}
