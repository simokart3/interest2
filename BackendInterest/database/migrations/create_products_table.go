package migrations

import (
	"time"

	"gorm.io/gorm"
)

type Products struct {
	ID             uint      `gorm:"primary key;autoIncrement" json:"id"`
	ProductName    *string   `gorm:"type:varchar(255)" json:"productname"`
	Sku            *string   `gorm:"type:varchar(255)" json:"sku"`
	AttributeId    uint      `json:"attributeid"`
	TypeProductId  *string   `gorm:"type:varchar(255)" json:"typeproductid"`
	SizeProductId  *string   `gorm:"type:varchar(255)" json:"sizeproductid"`
	ColorProductId *string   `gorm:"type:varchar(255)" json:"colorproductid"`
	Image          *string   `gorm:"type:text" json:"image"`
	Price          float32   `gorm:"type:decimal(12,2);" json:"price"`
	SpecialPrice   float32   `gorm:"type:decimal(12,2);" json:"specialprice"`
	StockId        uint      `json:"stockid"`
	Qty            float32   `gorm:"type:decimal(12,4);" json:"qty"`
	IsInStock      *uint     `json:"isinstock"`
	StockStatus    *uint     `json:"stockstatus"`
	Created_at     time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	Updated_at     time.Time `json:"updated_at"`
}

func MigrateProducts(db *gorm.DB) error {
	err := db.AutoMigrate(&Products{})
	return err
}
