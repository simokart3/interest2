package migrations

import (
	"gorm.io/gorm"
)

type StockStatus struct {
	ProductId   *uint   ` json:"productid"`
	StockId     *uint   ` json:"stockid"`
	Qty         float32 `gorm:"type:decimal(12,4);" json:"qty"`
	StockStatus *uint   ` json:"stockstatus"`
}

func MigrateStockStatus(db *gorm.DB) error {
	err := db.AutoMigrate(&StockStatus{})
	return err
}
