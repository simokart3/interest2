package middlewares

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/codetagon/crud_golang_reactjs/auth"
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
)

var (
	Logger *log.Logger
)

func customLogger() {
	errorLogPath := "error/error.log"
	errorfile, err := os.OpenFile(errorLogPath, os.O_WRONLY|os.O_APPEND, 0644)

	if err != nil {
		log.Println("Error", err)
	}
	Logger = log.New(errorfile, "Error :", log.Ldate|log.Ltime|log.Lshortfile)
}

func Auth() fiber.Handler {
	return func(c *fiber.Ctx) error {
		tokenString := c.Get("Authorization")
		if tokenString == "" {
			return c.Status(http.StatusBadRequest).JSON(fiber.Map{"error": "request does not contain an access token"})
		}

		err := auth.ValidateToken(tokenString)
		if err != nil {
			return c.Status(http.StatusBadRequest).JSON(fiber.Map{"error": err.Error()})
		}

		return c.Next()
	}
}

func UserAuth(c *fiber.Ctx) error {
	customLogger()
	tokenString := c.Cookies("UserAuthorization")

	if tokenString == "" {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{"Massage": "Invalid access, User logout"})
	}

	//decode / validate it
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("SECERET")), nil
	})

	if err != nil {
		return c.Status(http.StatusBadGateway).JSON(fiber.Map{"status": "false", "Massage": "Error occured while token genaration"})
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if float64(time.Now().Unix()) > claims["exp"].(float64) {
			return c.Status(http.StatusUnauthorized).SendString("Token expired")
		}

		c.Set("userid", "tes")
		Logger.Print(claims["sub"])
		return c.Next()
	} else {
		return c.Status(http.StatusUnauthorized).SendString("Invalid token")
	}
}
