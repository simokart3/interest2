package models

type ColorProduct struct {
	Color string `json:"color" validate:"required"`
}
