package bootstrap

import (
	"log"
	"os"

	"github.com/codetagon/crud_golang_reactjs/database/migrations"
	"github.com/codetagon/crud_golang_reactjs/database/models"
	"github.com/codetagon/crud_golang_reactjs/database/storage"
	"github.com/codetagon/crud_golang_reactjs/repository"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/joho/godotenv"
)

var (
	Logger *log.Logger
)

func customLogger() {
	errorLogPath := "error/error.log"
	errorfile, err := os.OpenFile(errorLogPath, os.O_WRONLY|os.O_APPEND, 0644)

	if err != nil {
		log.Println("Error", err)
	}
	Logger = log.New(errorfile, "Error :", log.Ldate|log.Ltime|log.Lshortfile)
}

func InitializeApp(app *fiber.App) {
	customLogger()
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}

	config := &storage.Config{
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		Password: os.Getenv("DB_PASSWORD"),
		User:     os.Getenv("DB_USER"),
		DBName:   os.Getenv("DB_DBNAME"),
		SSLMode:  os.Getenv("DB_SSLMODE"),
	}
	db, err := storage.NewConnection(config)

	if err != nil {
		log.Fatal("Could Not Connect Database")
	}

	err = migrations.MigrateProducts(db)
	//migration for users only
	db.AutoMigrate(&models.Users{})
	db.AutoMigrate(&models.Address{})

	if err != nil {
		log.Fatal("Could not migrate db")
	}
	repo := repository.Repository{
		DB: db,
	}
	app.Use(cors.New(cors.Config{AllowCredentials: true}))
	repo.SetupRoutes(app)
	app.Listen(":8081")
}
