package repository

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"net/http"
	"net/smtp"
	"os"
	"strconv"

	"github.com/codetagon/crud_golang_reactjs/database/models"
	"github.com/gofiber/fiber/v2"
)

type User_otp struct {
	otp   string
	email string
}

func VerifyOTP(email string) string {
	Otp, err := getRandNum()
	if err != nil {
		panic(err)
	}

	sendMail(email, Otp)
	return Otp
}

func sendMail(email string, otp string) {
	fmt.Println("Email : ", email, "Otp : ", otp)
	// Sender data
	from := os.Getenv("SMTP_EMAIL")
	password := os.Getenv("SMTP_PASSWORD")

	// Receiver email address.
	to := []string{
		email,
	}

	// smtp server configuration
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	// message.

	// Authentication
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Sending email
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, []byte(otp+" is your otp"))
	if err != nil {
		fmt.Println(err)
		return
	}
}

func getRandNum() (string, error) {
	nBig, e := rand.Int(rand.Reader, big.NewInt(8999))
	if e != nil {
		return "", e
	}
	return strconv.FormatInt(nBig.Int64()+1000, 10), nil
}

// ----------Otp Validation------>
func (r *Repository) getOtpValidation(context *fiber.Ctx) error {
	var user_otp User_otp
	var UserData models.Users
	if err := context.BodyParser(&user_otp); err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed", "Error": err.Error()})
	}

	//check jika otp dan email
	record := r.DB.First(&UserData, "otp LIKE ? AND email LIKE ?", user_otp.otp, user_otp.email)
	if record.Error != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Goto /signup/otpvalidate", "Error": record.Error.Error()})
	}
	context.Status(http.StatusOK).JSON(fiber.Map{"message": "New User Successfully Registered"})
	return nil
}
