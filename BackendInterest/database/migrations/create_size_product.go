package migrations

import (
	"gorm.io/gorm"
)

type SizeProduct struct {
	SizeId uint    `gorm:"primary key;autoIncrement" json:"size_id"`
	Size   *string `json:"size"`
}

func MigrateSizeProduct(db *gorm.DB) error {
	err := db.AutoMigrate(&SizeProduct{})
	return err
}
