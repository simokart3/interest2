package models

type SizeProduct struct {
	Size string `json:"size" validate:"required"`
}
