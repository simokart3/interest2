package models

type StocksStatus struct {
	ProductId   uint    `json:"productid"`
	StockId     uint    `json:"stockid"`
	Qty         float32 `gorm:"type:decimal(12,4);" json:"qty" `
	StockStatus uint    `json:"stockstatus" `
}
