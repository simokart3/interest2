package migrations

import (
	"gorm.io/gorm"
)

type ColorProduct struct {
	ColorId uint    `gorm:"primary key;autoIncrement" json:"color_id"`
	Color   *string `json:"color"`
}

func MigrateColorProduct(db *gorm.DB) error {
	err := db.AutoMigrate(&ColorProduct{})
	return err
}
