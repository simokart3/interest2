package models

type TypeProduct struct {
	TypeName string `json:"typename" validate:"required"`
}
