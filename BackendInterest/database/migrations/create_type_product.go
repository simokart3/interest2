package migrations

import (
	"gorm.io/gorm"
)

type TypeProduct struct {
	TypeId   uint    `gorm:"primary key;autoIncrement" json:"type_id"`
	TypeName *string `json:"typename"`
}

func MigrateTypeProduct(db *gorm.DB) error {
	err := db.AutoMigrate(&TypeProduct{})
	return err
}
