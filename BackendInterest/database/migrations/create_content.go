package migrations

import (
	"gorm.io/gorm"
)

type Content struct {
	ID      uint    `gorm:"primary key;autoIncrement" json:"id"`
	Subject *string `gorm:"type:varchar(255)" json:"subject"`
	Image   *string `gorm:"type:text" json:"image"`
	Desc    *string `gorm:"type:text" json:"description"`
}

func MigrateContent(db *gorm.DB) error {
	err := db.AutoMigrate(&Content{})
	return err
}
