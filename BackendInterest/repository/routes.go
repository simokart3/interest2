package repository

import (
	"github.com/codetagon/crud_golang_reactjs/middlewares"
	"github.com/gofiber/fiber/v2"
)

func (repo *Repository) SetupRoutes(app *fiber.App) {

	api := app.Group("/api")
	{

		//Users Routes
		api.Post("/register", repo.register)
		api.Post("/login", repo.usersLogin)
		api.Post("/token", repo.generateToken)
		api.Get("/logout", middlewares.UserAuth, repo.UserSignout)

		//Users Profile Routes
		api.Get("/viewprofile", middlewares.UserAuth, repo.showUsersDetails)
		api.Post("/addaddress", middlewares.UserAuth, repo.addAddress)
		api.Post("/editaddress/:id", middlewares.UserAuth, repo.editAddress)
		api.Get("/searchaddress/:id", middlewares.UserAuth, repo.showAddress)
		api.Post("/userchangepassword", middlewares.UserAuth, repo.UserChangePassword)
		api.Put("/userchangepassword/updatepassword", middlewares.UserAuth, repo.Updatepassword)
		api.Put("/editprofile", middlewares.UserAuth, repo.EditUserProfilebyUser)
		// api.Get("/product/wishlist", middlewares.UserAuth, repo.Wishlist)

		//api for search product
		api.Get("/products/search", repo.SearchProductByQuery)

		//secure token if not generated or expires token
		secured := api.Group("/secured").Use(middlewares.Auth())
		{
			secured.Get("/ping", repo.pong)
			//api for users
			api.Get("/users", repo.getUsers)
			api.Post("/users", repo.createUsers)
			api.Patch("/users/:id", repo.updateUsers)
			api.Delete("/users/:id", repo.deleteUsers)
			api.Get("/users/:id", repo.getUsersByUsersId)

			//api for products
			secured.Get("/products", repo.getProducts)
			secured.Post("/products", repo.createProducts)
			secured.Patch("/products/:id", repo.updateProducts)
			secured.Delete("/products/:id", repo.deleteProducts)

			//api type products
			secured.Get("/typeproducts", repo.getTypeProducts)
			secured.Post("/typeproducts", repo.createTypeProducts)
			secured.Patch("/typeproducts/:type_id", repo.updateTypeProducts)
			secured.Delete("/typeproducts/:type_id", repo.deleteTypeProducts)

			//api size products
			secured.Get("/sizeproducts", repo.getSizeProducts)
			secured.Post("/sizeproducts", repo.createSizeProducts)
			secured.Patch("/sizeproducts/:size_id", repo.updateSizeProducts)
			secured.Delete("/sizeproducts/:size_id", repo.deleteSizeProducts)

			//api color products
			secured.Get("/colorproducts", repo.getColorProducts)
			secured.Post("/colorproducts", repo.createColorProducts)
			secured.Patch("/colorproducts/:color_id", repo.updateColorProducts)
			secured.Delete("/colorproducts/:color_id", repo.deleteColorProducts)

			//api for CreateContent
			secured.Get("/content", repo.getContent)
			secured.Post("/content", repo.createContent)
			secured.Patch("/content/:id", repo.updateContent)
			secured.Delete("/content/:id", repo.deleteContent)
		}
	}

}
