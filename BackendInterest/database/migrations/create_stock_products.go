package migrations

import (
	"gorm.io/gorm"
)

type Stocks struct {
	ItemId    uint    `gorm:"primary key;autoIncrement" json:"id"`
	ProductId *uint   ` json:"productid"`
	StockId   *uint   ` json:"stockid"`
	Qty       float32 `gorm:"type:decimal(12,4);" json:"qty"`
	IsInStock *uint   ` json:"isinstock"`
}

func MigrateStocks(db *gorm.DB) error {
	err := db.AutoMigrate(&Stocks{})
	return err
}
