package models

import "time"

type Products struct {
	ProductName    string    `json:"productname" validate:"required,min=3,max=40"`
	Sku            string    `json:"sku" validate:"required,min=3,max=32"`
	AttributeId    uint      `json:"attributeid" validate:"required"`
	TypeProductId  string    `json:"typeproductid" validate:"required,min=3,max=40"`
	SizeProductId  string    `json:"sizeproductid" validate:"required,min=1,max=40"`
	ColorProductId string    `json:"colorproductid" validate:"required,min=3,max=40"`
	Image          string    `json:"image"`
	Price          float32   `json:"price" validate:"required"`
	SpecialPrice   float32   `json:"specialprice" `
	StockId        uint      `json:"stockid" validate:"required"`
	Qty            float32   `json:"qty" validate:"required"`
	IsInStock      *uint     `json:"isinstock" validate:"required"`
	StockStatus    *uint     `json:"stockstatus" validate:"required"`
	Created_at     time.Time `json:"created_at"`
	Updated_at     time.Time `json:"updated_at"`
}
