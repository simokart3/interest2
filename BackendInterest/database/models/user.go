package models

import (
	"time"
)

type Users struct {
	ID          uint      `gorm:"primaryKey;unique" json:"id"`
	FirstName   string    `json:"first_name"  gorm:"not null" validate:"required,min=2,max=50"`
	LastName    string    `json:"last_name"    gorm:"not null"    validate:"required,min=1,max=50"`
	Email       string    `json:"email"   gorm:"not null;unique"  validate:"email,required"`
	Username    string    `json:"username" gorm:"unique"`
	Password    string    `json:"password" gorm:"not null"  validate:"required"`
	IsAdmin     bool      `JSON:"isadmin" gorm:"default:false"`
	PhoneNumber int       `json:"phone"   gorm:"not null;unique"`
	Otp         string    `JSON:"otp"`
	Isblocked   bool      `JSON:"isblocked" gorm:"default:false"`
	Created_At  time.Time `json:"created_at"`
	Updated_At  time.Time `json:"updated_at"`
}

type Address struct {
	Addressid uint `JSON:"addressid" gorm:"primarykey;unique"`

	User   Users `gorm:"ForeignKey:Userid"`
	Userid uint  `JSON:"uid"`

	Name       string `JSON:"name" gorm:"not null"`
	Phoneno    string `JSON:"phoneno" gorm:"not null"`
	Houseno    string `JSON:"houseno" gorm:"not null"`
	Area       string `JSON:"area" gorm:"not null"`
	Landmark   string `JSON:"landmark" gorm:"not null"`
	City       string `JSON:"city" gorm:"not null"`
	Pincode    string `JSON:"pincode" gorm:"not null"`
	District   string `JSON:"district" gorm:"not null"`
	State      string `JSON:"state" gorm:"not null"`
	Country    string `JSON:"country" gorm:"not null"`
	Defaultadd bool   `JSON:"defaultadd" gorm:"default:false"`
}
