import React, {useState, useEffect} from "react"
import { Link, useParams } from "react-router-dom"
import { API_URL } from "../config"

export default function ViewUser() {
    let {id} = useParams()
    const [users, setUsers] = useState({})
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        const fetchUser = async () => {
             setLoading(true)
             try {
                const response = await fetch(`${API_URL}users/${id}`)
                const json = await response.json();
                setUsers(json.data)
                setLoading(false)
             } catch (error) {
                console.log("error",error)
                setLoading(false)
             }
        }

        fetchUser()
    }, [id])
    return(
        <div>
            {   !loading ?
                <div className="flex justify-center">
                    <div className="lg:w-1/3 w-full">
                        <div className="p-10">
                            <div className="mb-10 flex items-center justify-between">
                                <Link to="/"><h1 className="font-bold">Go Back</h1></Link>
                            </div>
                            <div className="bg-slate-100 rounded-lg px-5">
                                <div className="flex border-b py-4">
                                    <div className="mr-4 text-slate-400">Name</div>
                                    <div className="mr-4 text-slate-800 font-medium">{users.name}</div>
                                </div>
                            </div>
                            <div className="bg-slate-100 rounded-lg px-5">
                                <div className="flex border-b py-4">
                                    <div className="mr-4 text-slate-400">Email</div>
                                    <div className="mr-4 text-slate-800 font-medium">{users.email}</div>
                                </div>
                            </div>
                            <div className="bg-slate-100 rounded-lg px-5">
                                <div className="flex border-b py-4">
                                    <div className="mr-4 text-slate-400">Date</div>
                                    <div className="mr-4 text-slate-800 font-medium">{users.date}</div>
                                </div>
                            </div>
                            <div className="bg-slate-100 rounded-lg px-5">
                                <div className="flex border-b py-4">
                                    <div className="mr-4 text-slate-400">City</div>
                                    <div className="mr-4 text-slate-800 font-medium">{users.city}</div>
                                </div>
                            </div>
                            <div className="bg-slate-100 rounded-lg px-5">
                                <div className="flex border-b py-4">
                                    <div className="mr-4 text-slate-400">Country</div>
                                    <div className="mr-4 text-slate-800 font-medium">{users.country}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                : ''
            }
        </div>
    )
}