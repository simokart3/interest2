package models

type Content struct {
	Subject string `json:"subject" validate:"required"`
	Image   string `json:"banner" validate:"required"`
	Desc    string `json:"description" validate:"required"`
}
