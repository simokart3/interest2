package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/codetagon/crud_golang_reactjs/auth"
	"github.com/codetagon/crud_golang_reactjs/database/migrations"
	"github.com/codetagon/crud_golang_reactjs/database/models"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/morkid/paginate"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/go-playground/validator.v9"
)

type ErrorResponse struct {
	FailedField string
	Tag         string
	Value       string
}

var validate = validator.New()

// declared request
type TokenRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// declared request login
type UserData struct {
	Email    string
	Password string
}

type checkUserData struct {
	Firstname       string
	Lastname        string
	Email           string
	Password        string
	ConfirmPassword string
	PhoneNumber     int
	Otp             string
}

var (
	Logger *log.Logger
)

func customLogger() {
	errorLogPath := "error/error.log"
	errorfile, err := os.OpenFile(errorLogPath, os.O_WRONLY|os.O_APPEND, 0644)

	if err != nil {
		log.Println("Error", err)
	}
	Logger = log.New(errorfile, "Error :", log.Ldate|log.Ltime|log.Lshortfile)
}

func validateStruct(class interface{}) []*ErrorResponse {
	var errors []*ErrorResponse
	err := validate.Struct(class)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ErrorResponse
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}
	return errors
}

// function login user
func (r *Repository) usersLogin(context *fiber.Ctx) error {
	var user UserData
	customLogger()
	if err := context.BodyParser(&user); err != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"message": "Data binding error", "error": err.Error()})
	}
	var checkuser models.Users
	db := r.DB
	result := db.First(&checkuser, "email LIKE ?", user.Email)

	if checkuser.Isblocked == true {
		context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"status": " Authorization",
			"message": "User blocked by admin"})
	}
	if result.Error != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "user not exist"})
		return result.Error
	}
	credentialErrors := CheckPassword(user.Password, checkuser.Password)

	if credentialErrors != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Invalid Password"})
	}

	str := strconv.Itoa(int(checkuser.ID))
	tokenString := auth.TokenGeneration(str)
	context.Cookie(&fiber.Cookie{
		Name:     "UserAuthorization",
		Value:    tokenString,
		Expires:  time.Now().Add(30 * 24 * time.Hour),
		HTTPOnly: true,
		SameSite: "Lax",
	})

	context.Status(http.StatusOK).JSON(fiber.Map{"message": "User login successfully"})
	return nil
}

// function register user
func (r *Repository) register(context *fiber.Ctx) error {
	user := models.Users{}
	customLogger()
	if err := context.BodyParser(&user); err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed"})
	}
	errors := validateStruct(user)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}

	hashoutput, hasherr := HashPassword(user.Password)

	if hasherr != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"error": hasherr.Error()})
	}

	db := r.DB
	result := db.First(&user, "email LIKE ?", user.Email).Error
	if result != nil {
		registerPayload := models.Users{
			FirstName:   user.FirstName,
			LastName:    user.LastName,
			Email:       user.Email,
			Username:    user.Username,
			Password:    hashoutput,
			PhoneNumber: user.PhoneNumber,
		}
		otp := VerifyOTP(user.Email)
		if err := r.DB.Create(&registerPayload).Error; err != nil {
			return context.Status(http.StatusBadRequest).JSON(fiber.Map{"status": "error", "message": "couldn't create user", "data": err})
		} else {
			db.Model(&user).Where("email LIKE ?", user.Email).Update("otp", otp)
			context.Status(http.StatusAccepted).JSON(fiber.Map{"message": "Go to /signup/otpvalidate"})
		}
		context.Status(http.StatusOK).JSON(fiber.Map{"message": "User has been added", "data": registerPayload})
		return nil
	} else {
		return context.Status(http.StatusBadRequest).JSON(fiber.Map{"Error": "User already Exist"})
	}

}

// function logout user
func (r *Repository) UserSignout(context *fiber.Ctx) error {
	context.Cookie(&fiber.Cookie{
		Name:     "UserAuthorization",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
		SameSite: "Lax",
	})
	context.Status(http.StatusOK).JSON(fiber.Map{"message": "User Succesfully Logout"})
	return nil
}

// function generate token user
func (r *Repository) generateToken(context *fiber.Ctx) error {
	var request TokenRequest
	var user models.Users
	if err := context.BodyParser(&request); err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed", "Error": err.Error()})
	}

	// check jika email ada dan password sesuai
	record := r.DB.Where("email = ?", request.Email).First(&user)
	if record.Error != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Check Record Failed", "Error": record.Error.Error()})
	}
	credentialErrors := CheckPassword(request.Password, user.Password)

	if credentialErrors != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Invalid Credentials"})
	}
	tokenString, err := auth.GenerateJWT(user.Email, user.Username)
	if err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"error": err.Error()})
	}
	context.Status(http.StatusOK).JSON(fiber.Map{"message": "Success generate Token", "token": tokenString})
	return nil
}

func (r *Repository) pong(context *fiber.Ctx) error {
	return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Pong"})
}

// function api get users details
func (r *Repository) showUsersDetails(context *fiber.Ctx) error {
	var usersData models.Users
	id, err := strconv.Atoi(context.Get("userid"))

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"Error": "Error in string conversion"})
		return err
	}
	result := r.DB.First(&usersData, "id = ?", id)
	if result.Error != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "users not exist"})
	}
	context.Status(http.StatusOK).JSON(fiber.Map{
		"First Name":   usersData.FirstName,
		"Last name":    usersData.LastName,
		"Email":        usersData.Email,
		"Phone number": usersData.PhoneNumber})
	return nil
}

// function api get User
func (r *Repository) getUsers(context *fiber.Ctx) error {
	customLogger()

	db := r.DB
	model := db.Model(&migrations.Users{})
	pg := paginate.New(&paginate.Config{
		DefaultSize:        20,
		CustomParamEnabled: true,
	})

	page := pg.With(model).Request(context.Request()).Response(&[]migrations.Users{})

	context.Status(http.StatusOK).JSON(&fiber.Map{
		"data": page,
	})
	return nil
}

// function api create user
func (r *Repository) createUsers(context *fiber.Ctx) error {

	user := models.Users{}
	err := context.BodyParser(&user)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(user)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}
	if err := r.DB.Create(&user).Error; err != nil {
		return context.Status(http.StatusBadRequest).JSON(fiber.Map{"status": "error", "message": "couldn't create user", "data": err})
	}
	context.Status(http.StatusOK).JSON(fiber.Map{"message": "User has been added", "data": user})
	return nil
}

// function api update user
func (r *Repository) updateUsers(context *fiber.Ctx) error {
	user := models.Users{}
	err := context.BodyParser(&user)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(user)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}

	db := r.DB
	id := context.Params("id") // param id must same with routes

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	if db.Model(&user).Where("id = ? ", id).Updates(&user).RowsAffected == 0 {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Could Not get User Profile"})
		return nil
	}

	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "user successfully update"})
	return nil
}

// function api delete user
func (r *Repository) deleteUsers(context *fiber.Ctx) error {
	usersModel := migrations.Users{}
	id := context.Params("id")

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	err := r.DB.Delete(usersModel, id)

	if err.Error != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "could not delete user"})
		return err.Error
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "user successfully delete"})
	return nil
}

// function api get user by id
func (r *Repository) getUsersByUsersId(context *fiber.Ctx) error {
	usersModel := &migrations.Users{}
	id := context.Params("id")

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	err := r.DB.Where("id = ?", id).First(usersModel).Error

	if err != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "could not get user by id"})
		return err
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "successfully get user", "data": usersModel})
	return nil
}

// function add address users
func (r *Repository) addAddress(context *fiber.Ctx) error {
	customLogger()
	id, err := strconv.Atoi(context.Get("userid"))
	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"Error": "Error in string conversion"})
		return err
	}
	user := models.Users{}
	address := models.Address{}
	if context.BodyParser(&address) != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"Error": "Error in Binding the JSON"})

	}
	Logger.Println(user.FirstName)
	r.DB.Model(&models.Address{}).Where("userid = ?", id).Update("defaultadd", false)
	address.Userid = uint(id)
	result := r.DB.Create(&address)
	if result.Error != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "could not add address"})
		return result.Error
	}
	r.DB.Model(&address).Where("addressid = ?", address.Addressid).Updates(map[string]interface{}{
		"defaultadd": true,
		"name":       user.FirstName,
	})
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "successfully Add address"})
	return nil
}

// function edit addresss users
func (r *Repository) editAddress(context *fiber.Ctx) error {
	id := context.Params("id")
	usersAddress := models.Address{}

	if err := context.BodyParser(&usersAddress); err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Error in binding Json Data", "error": err.Error()})
	}
	str, err := strconv.Atoi(id)
	if err != nil {
		context.Status(http.StatusInternalServerError).JSON(&fiber.Map{"Error": err.Error()})
		return err
	}

	// checkUserId := r.DB.First(&usersAddress, "userid = ?", id)

	// if checkUserId.Error != nil {
	// 	return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "userid not exist"})
	// }
	usersAddress.Userid = uint(str)
	db := r.DB
	result := db.Model(usersAddress).Where("userid = ?", id).Updates(models.Address{
		Name:     usersAddress.Name,
		Phoneno:  usersAddress.Phoneno,
		Houseno:  usersAddress.Houseno,
		Area:     usersAddress.Area,
		Landmark: usersAddress.Landmark,
		City:     usersAddress.City,
		Pincode:  usersAddress.Pincode,
		District: usersAddress.District,
		State:    usersAddress.State,
		Country:  usersAddress.Country,
	})
	if result.Error != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"Error": result.Error.Error()})
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "successfully Update address",
		"updated address": fiber.Map{
			"Name":     usersAddress.Name,
			"Houseno":  usersAddress.Houseno,
			"Phoneno":  usersAddress.Phoneno,
			"Area":     usersAddress.Area,
			"Landmark": usersAddress.Landmark,
			"City":     usersAddress.City,
			"Pincode":  usersAddress.Pincode,
			"District": usersAddress.District,
			"State":    usersAddress.State,
			"Country":  usersAddress.Country,
		},
	})
	return nil

}

// function api show address
func (r *Repository) showAddress(context *fiber.Ctx) error {
	id, _ := strconv.Atoi(context.Params("id"))
	usersAddress := models.Address{}

	db := r.DB
	var count int64
	result := db.Raw("SELECT * from addresses where userid = ?", id).Scan(&usersAddress).Count(&count)
	if count == 0 {
		return context.Status(http.StatusInternalServerError).JSON(&fiber.Map{"message": "Users Not Found"})
	}

	if result.Error != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"Error": result.Error.Error()})
	}

	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "successfully show address",
		"updated address": fiber.Map{
			"Name":     usersAddress.Name,
			"Houseno":  usersAddress.Houseno,
			"Phoneno":  usersAddress.Phoneno,
			"Area":     usersAddress.Area,
			"Landmark": usersAddress.Landmark,
			"City":     usersAddress.City,
			"Pincode":  usersAddress.Pincode,
			"District": usersAddress.District,
			"State":    usersAddress.State,
			"Country":  usersAddress.Country,
		},
	})
	return nil
}

// function user change password
func (r *Repository) UserChangePassword(context *fiber.Ctx) error {
	var userEnterData checkUserData
	if err := context.BodyParser(&userEnterData); err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Error in binding Json Data", "error": err.Error()})
	}

	if userEnterData.Password != userEnterData.ConfirmPassword {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"message": "Password not match"})
	}

	id, err := strconv.Atoi(context.Get("userid"))

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"Error": "Error in string conversion"})
		return err
	}
	var userData models.Users
	result := r.DB.First(&userData, "id = ?", id)
	if result.Error != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "user not exist"})
	}
	err = CheckPassword(userEnterData.Password, userData.Password)
	if err != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "false", "error": "Password is incorrect"})
	} else {
		return context.Status(http.StatusOK).JSON(&fiber.Map{"message": "Go to /userchangepassword/updatepassword"})
	}
}

// function api update passwords user
func (r *Repository) Updatepassword(context *fiber.Ctx) error {
	var userEnterData checkUserData
	var userData models.Users

	if err := context.BodyParser(&userEnterData); err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Error in binding Json Data", "error": err.Error()})
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(userEnterData.Password), 10)
	if err != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "false", "error": "Hashing password error"})
	}
	id, err := strconv.Atoi(context.Get("userid"))
	if err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"Error": "Error in string conversion"})
	}
	result := r.DB.First(&userData, "id = ? ", id)
	if result.Error != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "user not exist"})
	}
	r.DB.Model(&userData).Where("id = ?", id).Update("password", hash)
	return context.Status(http.StatusOK).JSON(&fiber.Map{"message": "succesfully update password"})
}

// function api edit user profile
func (r *Repository) EditUserProfilebyUser(context *fiber.Ctx) error {
	var userEnterData checkUserData
	if err := context.BodyParser(&userEnterData); err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Error in binding Json Data", "error": err.Error()})
	}
	var userData models.Users
	id, err := strconv.Atoi(context.Get("userid"))
	if err != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"error": "Error in string conversion"})
	}

	result := r.DB.First(&userData, "id = ?", id)
	if result.Error != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "user not exist"})
	}
	result = r.DB.Model(&userData).Updates(models.Users{
		FirstName:   userData.FirstName,
		LastName:    userData.LastName,
		PhoneNumber: userData.PhoneNumber,
	})
	if result.Error != nil {
		return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"error": result.Error.Error()})
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "successfully update the profile",
		"updated data": fiber.Map{
			"First Name": userData.FirstName,
			"Last Name":  userData.LastName,
			"Phone No":   userData.PhoneNumber,
		},
	})
	return nil
}

// add product to wishlist
// func (r *Repository) Wishlist(context *fiber.Ctx) error {
// 	userId, err := strconv.Atoi(context.Get("userid"))
// 	if err != nil {
// 		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"error": "Error in string conversion"})
// 	}
// 	productId, err := strconv.Atoi(context.Query("pid"))
// 	if err != nil {
// 		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"error": "Error in string conversion"})
// 	}
// }

// function api getProducts
func (r *Repository) getProducts(context *fiber.Ctx) error {
	db := r.DB
	model := db.Model(&migrations.Products{})
	pg := paginate.New(&paginate.Config{
		DefaultSize:        20,
		CustomParamEnabled: true,
	})

	page := pg.With(model).Request(context.Request()).Response(&[]migrations.Products{})

	context.Status(http.StatusOK).JSON(&fiber.Map{
		"data": page,
	})
	return nil
}

// function api create product
func (r *Repository) createProducts(context *fiber.Ctx) error {
	customLogger()

	product := models.Products{}
	err := context.BodyParser(&product)

	file, errorfile := context.FormFile("image")

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed"})
		return err
	}
	if errorfile != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "File Error", "error": errorfile.Error()})
	}
	if mapValues, errHandle := handleImage(file, errorfile, context); errHandle == nil {

		errors := validateStruct(product)

		if errors != nil {
			return context.Status(fiber.StatusBadRequest).JSON(errors)
		}

		now := time.Now()
		newProducts := models.Products{
			ProductName:    product.ProductName,
			Sku:            product.Sku,
			AttributeId:    product.AttributeId,
			TypeProductId:  product.TypeProductId,
			SizeProductId:  product.SizeProductId,
			ColorProductId: product.ColorProductId,
			Image:          mapValues["imageUrl"],
			Price:          product.Price,
			SpecialPrice:   product.SpecialPrice,
			StockId:        product.StockId,
			Qty:            product.Qty,
			IsInStock:      product.IsInStock,
			StockStatus:    product.StockStatus,
			Created_at:     now,
			Updated_at:     now,
		}

		//check if name AND sku exist
		record := r.DB.Where("product_name = @name AND sku = @sku ", sql.Named("name", product.ProductName), sql.Named("sku", product.Sku)).First(&product)

		if record.RowsAffected > 0 {
			if r.DB.Model(&product).Where("product_name = ? ", product.ProductName).Updates(&newProducts).RowsAffected == 0 {
				return context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Could Not Update Product Data"})
			}
			context.Status(http.StatusOK).JSON(fiber.Map{"message": "product has been Updated", "data": newProducts})
			return nil
		}

		if err := r.DB.Create(&newProducts).Error; err != nil {
			return context.Status(http.StatusBadRequest).JSON(fiber.Map{"status": "error", "message": "couldn't create products", "data": err})
		}
		context.Status(http.StatusOK).JSON(fiber.Map{"message": "product has been added", "data": newProducts})

		return nil
	} else {
		return errHandle
	}

}

// function api delete products
func (r *Repository) deleteProducts(context *fiber.Ctx) error {
	product := migrations.Products{}
	id := context.Params("id")

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	err := r.DB.Delete(product, id)

	if err.Error != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "could not delete products"})
		return err.Error
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "products successfully delete"})
	return nil
}

// func update products golang
func (r *Repository) updateProducts(context *fiber.Ctx) error {

	product := models.Products{}
	err := context.BodyParser(&product)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"message": "Request Failed"})
		return err
	}

	db := r.DB
	id := context.Params("id") // param id must same with routes
	file, errorfile := context.FormFile("image")

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}

	if errorfile != nil {
		return context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "File Error", "error": errorfile.Error()})
	}

	if mapValues, errHandle := handleImage(file, errorfile, context); errHandle == nil {
		errors := validateStruct(product)

		if errors != nil {
			return context.Status(fiber.StatusBadRequest).JSON(errors)
		}
		updatesPayload := make(map[string]interface{})

		if product.ProductName != "" {
			updatesPayload["ProductName"] = product.ProductName
		}
		if product.Sku != "" {
			updatesPayload["Sku"] = product.Sku
		}
		if product.AttributeId != 0 {
			updatesPayload["AttributeId"] = product.AttributeId
		}
		if product.TypeProductId != "" {
			updatesPayload["TypeProductId"] = product.TypeProductId
		}
		if product.SizeProductId != "" {
			updatesPayload["SizeProductId"] = product.SizeProductId
		}
		if product.ColorProductId != "" {
			updatesPayload["ColorProductId"] = product.ColorProductId
		}
		if mapValues["imageUrl"] != "" {
			updatesPayload["Image"] = mapValues["imageUrl"]
		}
		if product.Price != 0 {
			updatesPayload["Price"] = product.Price
		}
		if product.SpecialPrice != 0 {
			updatesPayload["SpecialPrice"] = product.SpecialPrice
		}
		if product.StockId != 0 {
			updatesPayload["StockId"] = product.StockId
		}
		if product.Qty != 0 {
			updatesPayload["Qty"] = product.Qty
		}
		if *product.IsInStock != 0 {
			updatesPayload["IsInStock"] = product.IsInStock
		}
		if *product.StockStatus != 0 {
			updatesPayload["StockStatus"] = product.StockStatus
		}

		updatesPayload["Updated_at"] = time.Now()

		if db.Model(&product).Where("id = ? ", id).Updates(&updatesPayload).RowsAffected == 0 {
			context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Could Not get Product Data"})
			return nil
		}

		context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "Product successfully update"})
		return nil
	} else {
		return errHandle
	}

}

// function api get type products
func (r *Repository) getTypeProducts(context *fiber.Ctx) error {
	db := r.DB
	model := db.Model(&migrations.TypeProduct{})
	pg := paginate.New(&paginate.Config{
		DefaultSize:        20,
		CustomParamEnabled: true,
	})

	page := pg.With(model).Request(context.Request()).Response(&[]migrations.TypeProduct{})

	context.Status(http.StatusOK).JSON(&fiber.Map{
		"data": page,
	})
	return nil
}

// function api create type products
func (r *Repository) createTypeProducts(context *fiber.Ctx) error {

	typeProducts := models.TypeProduct{}
	err := context.BodyParser(&typeProducts)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(typeProducts)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}
	if err := r.DB.Create(&typeProducts).Error; err != nil {
		return context.Status(http.StatusBadRequest).JSON(fiber.Map{"status": "error", "message": "couldn't create type Product", "data": err})
	}
	context.Status(http.StatusOK).JSON(fiber.Map{"message": "Type has been added", "data": typeProducts})
	return nil
}

// function api update type products
func (r *Repository) updateTypeProducts(context *fiber.Ctx) error {
	typeProducts := models.TypeProduct{}
	err := context.BodyParser(&typeProducts)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(typeProducts)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}

	db := r.DB
	id := context.Params("type_id") // param id must same with routes

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	if db.Model(&typeProducts).Where("type_id = ? ", id).Updates(&typeProducts).RowsAffected == 0 {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Could Not get type Product"})
		return nil
	}

	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "type product successfully update"})
	return nil
}

// function api delete type product
func (r *Repository) deleteTypeProducts(context *fiber.Ctx) error {
	typeModel := migrations.TypeProduct{}
	id := context.Params("type_id")

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	err := r.DB.Delete(typeModel, id)

	if err.Error != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "could not delete type Product"})
		return err.Error
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "type Product successfully delete"})
	return nil
}

// function api get size products
func (r *Repository) getSizeProducts(context *fiber.Ctx) error {
	db := r.DB
	model := db.Model(&migrations.SizeProduct{})
	pg := paginate.New(&paginate.Config{
		DefaultSize:        20,
		CustomParamEnabled: true,
	})

	page := pg.With(model).Request(context.Request()).Response(&[]migrations.SizeProduct{})

	context.Status(http.StatusOK).JSON(&fiber.Map{
		"data": page,
	})
	return nil
}

// function api create size products
func (r *Repository) createSizeProducts(context *fiber.Ctx) error {

	sizeProducts := models.SizeProduct{}
	err := context.BodyParser(&sizeProducts)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(sizeProducts)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}
	if err := r.DB.Create(&sizeProducts).Error; err != nil {
		return context.Status(http.StatusBadRequest).JSON(fiber.Map{"status": "error", "message": "couldn't create size Product", "data": err})
	}
	context.Status(http.StatusOK).JSON(fiber.Map{"message": "Size has been added", "data": sizeProducts})
	return nil
}

// function api update size products
func (r *Repository) updateSizeProducts(context *fiber.Ctx) error {
	sizeProducts := models.SizeProduct{}
	err := context.BodyParser(&sizeProducts)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(sizeProducts)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}

	db := r.DB
	id := context.Params("size_id") // param id must same with routes

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	if db.Model(&sizeProducts).Where("size_id = ? ", id).Updates(&sizeProducts).RowsAffected == 0 {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Could Not get size Product"})
		return nil
	}

	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "size product successfully update"})
	return nil
}

// function api delete size product
func (r *Repository) deleteSizeProducts(context *fiber.Ctx) error {
	sizeModel := migrations.SizeProduct{}
	id := context.Params("size_id")

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	err := r.DB.Delete(sizeModel, id)

	if err.Error != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "could not delete size Product"})
		return err.Error
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "size Product successfully delete"})
	return nil
}

// function api get color products
func (r *Repository) getColorProducts(context *fiber.Ctx) error {
	db := r.DB
	model := db.Model(&migrations.ColorProduct{})
	pg := paginate.New(&paginate.Config{
		DefaultSize:        20,
		CustomParamEnabled: true,
	})

	page := pg.With(model).Request(context.Request()).Response(&[]migrations.ColorProduct{})

	context.Status(http.StatusOK).JSON(&fiber.Map{
		"data": page,
	})
	return nil
}

// function api create color products
func (r *Repository) createColorProducts(context *fiber.Ctx) error {

	colorProducts := models.ColorProduct{}
	err := context.BodyParser(&colorProducts)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(colorProducts)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}
	if err := r.DB.Create(&colorProducts).Error; err != nil {
		return context.Status(http.StatusBadRequest).JSON(fiber.Map{"status": "error", "message": "couldn't create color Product", "data": err})
	}
	context.Status(http.StatusOK).JSON(fiber.Map{"message": "Color has been added", "data": colorProducts})
	return nil
}

// function api update color products
func (r *Repository) updateColorProducts(context *fiber.Ctx) error {
	colorProducts := models.ColorProduct{}
	err := context.BodyParser(&colorProducts)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(colorProducts)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}

	db := r.DB
	id := context.Params("color_id") // param id must same with routes

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	if db.Model(&colorProducts).Where("color_id = ? ", id).Updates(&colorProducts).RowsAffected == 0 {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Could Not get color Product"})
		return nil
	}

	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "color product successfully update"})
	return nil
}

// function api delete color product
func (r *Repository) deleteColorProducts(context *fiber.Ctx) error {
	colorModel := migrations.ColorProduct{}
	id := context.Params("color_id")

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	err := r.DB.Delete(colorModel, id)

	if err.Error != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "could not delete color Product"})
		return err.Error
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "color Product successfully delete"})
	return nil
}

// function get content
func (r *Repository) getContent(context *fiber.Ctx) error {
	db := r.DB
	model := db.Model(&migrations.Content{})
	pg := paginate.New(&paginate.Config{
		DefaultSize:        20,
		CustomParamEnabled: true,
	})

	page := pg.With(model).Request(context.Request()).Response(&[]migrations.Content{})

	context.Status(http.StatusOK).JSON(&fiber.Map{
		"data": page,
	})
	return nil
}

// funct api create content
func (r *Repository) createContent(context *fiber.Ctx) error {

	content := models.Content{}
	err := context.BodyParser(&content)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(content)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}
	if err := r.DB.Create(&content).Error; err != nil {
		return context.Status(http.StatusBadRequest).JSON(fiber.Map{"status": "error", "message": "couldn't create Content", "data": err})
	}
	context.Status(http.StatusOK).JSON(fiber.Map{"message": "Content has been added", "data": content})
	return nil
}

// func api update content
func (r *Repository) updateContent(context *fiber.Ctx) error {
	content := models.Content{}
	err := context.BodyParser(&content)

	if err != nil {
		context.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"message": "Request Failed"})
		return err
	}

	errors := validateStruct(content)

	if errors != nil {
		return context.Status(fiber.StatusBadRequest).JSON(errors)
	}

	db := r.DB
	id := context.Params("id") // param id must same with routes

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	if db.Model(&content).Where("id = ? ", id).Updates(&content).RowsAffected == 0 {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Could Not get Content"})
		return nil
	}

	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "Content successfully update"})
	return nil
}

// func api delete content
func (r *Repository) deleteContent(context *fiber.Ctx) error {
	content := migrations.Content{}
	id := context.Params("id")

	if id == "" {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "Id Cannot Be empty"})
		return nil
	}
	err := r.DB.Delete(content, id)

	if err.Error != nil {
		context.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "error", "message": "could not delete Content"})
		return err.Error
	}
	context.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "message": "Content successfully delete"})
	return nil
}

// func api search product query
func (r *Repository) SearchProductByQuery(c *fiber.Ctx) error {
	customLogger()
	var searchProducts []models.Products
	products := models.Products{}
	queryParam := c.Query("ProductName")
	if queryParam == "" {
		Logger.Print("Empty Query")
		c.Set("Content-Type", "application/json")
		return c.Status(http.StatusBadRequest).JSON(&fiber.Map{"Error": "Invalid Search Index"})
	}
	ctx, cancel := context.WithTimeout(c.Context(), 100*time.Second)
	Logger.Print(ctx)
	defer cancel()
	if searchquerydb := r.DB.Model(&products).Where("product_name LIKE ?", "%"+queryParam+"%").Find(&searchProducts).Error; searchquerydb != nil {
		return c.Status(http.StatusBadRequest).JSON(&fiber.Map{"status": "invalid request"})
	}
	c.Status(http.StatusOK).JSON(&fiber.Map{"status": "success", "data": searchProducts})
	return nil
}

// function handle image golang
func handleImage(file *multipart.FileHeader, errorfile error, ctx *fiber.Ctx) (map[string]string, error) {
	if errorfile != nil {
		errorResponse := ctx.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"message": "File Error", "error": errorfile.Error()})
		return nil, errorResponse
	}
	uniqueId := uuid.New()
	fileName := strings.Replace(uniqueId.String(), "-", "", -1)
	fileExt := strings.Split(file.Filename, ".")[1]
	image := fmt.Sprintf("%s.%s", fileName, fileExt)

	errsave := ctx.SaveFile(file, fmt.Sprintf("./tmp/uploads_relative/%s", image))

	if errsave != nil {
		return nil, ctx.Status(http.StatusUnprocessableEntity).JSON(&fiber.Map{"stats": 500, "message": "no such file or directory", "data": nil})
	}
	imageUrl := fmt.Sprintf("http://127.0.0.1:8081/tmp/uploads_relative/%s", image)

	return map[string]string{
		"imageName": image,
		"imageUrl":  imageUrl,
	}, nil
}

// function handle Password register login
func HashPassword(password string) (string, error) {
	user := models.Users{}
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return "", err
	}
	user.Password = string(bytes)

	return user.Password, nil
}
func CheckPassword(providedPassword string, userPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(userPassword), []byte(providedPassword))
	if err != nil {
		return err
	}
	return nil
}
